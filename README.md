# Evaluation of VKN applied to Federated Learning

Simulation of vehicles and random event generation to evaluate VKN-assisted federated learning in terms of training information-aware client selection.

## How to use:

* First, to run a simulation, use the run_fed.py program. The parameters of the simulation can be tuned within the file.
A python pickle file will be generated which contains statistics about the simulation.

* Secondly, the analysis.py program can be used to visualize and plot various metrics about one generated pickle file.

## Dependencies:

The program uses TensorFlow federated to perform federated learning training.
Other used libraries include matplotlib for plot generation and numpy for data formatting.
